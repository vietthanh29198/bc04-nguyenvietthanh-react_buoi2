import React, { Component } from "react";
import Glass from "./Glass";

export default class ListGlass extends Component {
  render() {
    return (
      <div className="container">
        <div
          style={{
            background: "white",
          }}
          className="row mt-5"
        >
          {this.props.data.map((item, index) => {
            return (
              <div
                key={index.toString() + item.id}
                className="col-3"
                style={{ cursor: "pointer", border: "1px solid black" }}
              >
                <a>
                  <Glass
                    item={item}
                    handleSelectedGlass={this.props.handleSelectedGlass}
                  />
                </a>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
