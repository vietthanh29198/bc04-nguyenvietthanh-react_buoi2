import React, { Component } from "react";

export default class Glass extends Component {
  render() {
    let { url } = this.props.item;
    return (
      <>
        <img
          onClick={() => {
            this.props.handleSelectedGlass(this.props.item);
          }}
          className="w-100"
          src={`${url}`}
          alt=""
        />
      </>
    );
  }
}
