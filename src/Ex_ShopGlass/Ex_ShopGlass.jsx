import React, { Component } from "react";
import { dataGlass } from "./dataGlass";
import Glass from "./Glass";
import ListGlass from "./ListGlass";
import Model from "./Model";
import SelectedGlass from "./SelectedGlass";

export default class Ex_ShopGlass extends Component {
  state = {
    glassArr: dataGlass,
    selectedGlass: dataGlass[0],
  };

  handleSelectedGlass = (glass) => {
    this.setState({
      selectedGlass: glass,
    });
  };

  render() {
    return (
      <div className="App container">
        <div className="row">
          <div
            style={{ position: "relative" }}
            id="model-left"
            className="col-6"
          >
            <Model />
            <SelectedGlass detail={this.state.selectedGlass} />
          </div>
          <div id="model-right" className="col-6">
            <Model />
          </div>
        </div>
        <ListGlass
          data={this.state.glassArr}
          handleSelectedGlass={this.handleSelectedGlass}
        />
      </div>
    );
  }
}
