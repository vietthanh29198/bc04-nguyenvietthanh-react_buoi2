import React, { Component } from "react";
import Glass from "./Glass";

export default class SelectedGlass extends Component {
  render() {
    let { url, name, price, desc } = this.props.detail;
    return (
      <div>
        <img
          style={{
            position: "absolute",
            top: "8.3rem",
            left: "7.9rem",
            width: "45%",
            opacity: "0.8",
          }}
          src={`${url}`}
        />
        <div
          className="text-left"
          style={{
            backgroundColor: "white",
            position: "absolute",
            bottom: 0,
            left: "15px",
            width: "95%",
            opacity: "0.7",
          }}
        >
          <h3 className="text-primary">
            {name}
            <span className="text-danger"> ${price}</span>
          </h3>
          <h4>{desc}</h4>
        </div>
      </div>
    );
  }
}
