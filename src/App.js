import logo from "./logo.svg";
import "./App.css";
import Ex_ShopGlass from "./Ex_ShopGlass/Ex_ShopGlass";

function App() {
  return (
    <div
      style={{
        backgroundImage: "url(glassesImage/background.jpg)",
      }}
    >
      <Ex_ShopGlass />
    </div>
  );
}

export default App;
